#include "SDL_orbitalframebuffer.h"

int ORBITAL_CreateWindowFramebuffer(_THIS, SDL_Window * window,
                                       Uint32 * format,
                                       void ** pixels, int *pitch) {
    return 0;
}

int ORBITAL_UpdateWindowFramebuffer(_THIS, SDL_Window* window,
                                      const SDL_Rect* rects, int numrects) {
    return 0;
}

void ORBITAL_DestroyWindowFramebuffer(_THIS, SDL_Window* window) {

}